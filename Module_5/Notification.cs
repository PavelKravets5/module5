﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Module_5
{
    class Notification
    {
        public const int LIMIT_100 = 100;
        public const int LIMIT_3 = 3;
        public const int LIMIT_2 = 2;
        public const int LIMIT_0 = 0;

        public static bool ReadWithCheck(string str, int lowerLimit, int upperLimit,
            out int result)
        {
            bool check = true;
            check = int.TryParse(str, out result);
            if ( result < lowerLimit||result>upperLimit)
            {
                check = false;;
            }
            return check;
        }

        public static bool CheckAgreementOrDisagreement(ref string str)
        {   
            if(str==String.Empty||str==null)
            {
                return false;
            }
            str = str.ToLower();
            if (str == "y" || str == "n" || str=="yes" || str=="no")
            {
                return true;
            }
            return false;
        }

        public static void DisplayMessage(string message)
        {
            Console.Write(message);
        }
    }
}
