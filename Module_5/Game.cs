﻿using System;
using System.Collections.Generic;

namespace Module_5
{
    class Game
    {
        private const int MIN_NUMBERS_OF_POSSIBILITIES = 2;

        public delegate void GameHandler(string message);
        public event GameHandler Notification;

        private int _trapsNumber;
        private PositionOnThePlane _finishPosition;
        private int[,] _field;
        private int _fieldSize;
        private PositionOnThePlane _playerPosition;
        private int _playerLifes;
        
        public Game(int starPosNumber, int fieldSize, int trapsNumber)
        {
            _fieldSize = fieldSize;
            _trapsNumber = trapsNumber;
            _playerLifes = 10;
            _field = new int[_fieldSize, _fieldSize];

            switch (starPosNumber)
            {
                case 0:
                    {
                        _playerPosition = new PositionOnThePlane(0, 0);
                    }
                    break;
                case 1:
                    {
                        _playerPosition = new PositionOnThePlane(0, fieldSize - 1);
                    }
                    break;
                case 2:
                    {
                        _playerPosition = new PositionOnThePlane(fieldSize - 1, 0);
                    }
                    break;
                case 3:
                    {
                        _playerPosition = new PositionOnThePlane(fieldSize - 1, fieldSize - 1);
                    }
                    break;
            }

            _finishPosition = new PositionOnThePlane(fieldSize-1- _playerPosition.XPos, fieldSize-1- _playerPosition.YPos);
            
            List<PositionOnThePlane> positions = new List<PositionOnThePlane>();
            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    PositionOnThePlane position = new PositionOnThePlane(i, j);
                    if (_playerPosition != position && _finishPosition != position)       
                    {
                        positions.Add(position);
                    }
                }
            }
            int count = 0;
            Random rnd = new Random();
            while(count<_trapsNumber)
            {
                int index = rnd.Next(0, positions.Count);
                _field[positions[index].XPos,positions[index].YPos]= rnd.Next(1, 11);
                count++;
                positions.RemoveAt(index);
            }
        }

        public void ConsolePrintField()
        {         
            Notification?.Invoke($"На поле в рандомных точках расставлены ловушки, " +
                $"каждая отнимет у игрока рандомное кол-во очков жизни в пределах от 1 до 10. " +
                $"Цель: достич позиции отмеченной как \"fin\", " +
                $"Позиция игрока отмеченна как \"you\", поля, где Вы уже побывали " +
                $"будут помечены как \"sfl\" (от \"safely\"), " +
                $"на стартовой и финишной клетках - безопастно\n" +
               $"Осталось {_trapsNumber} рабочих ловушек.\n");

            Notification?.Invoke("\n");
            if (_field[_playerPosition.XPos, _playerPosition.YPos] > 0)
            {
                Notification?.Invoke($"Произошел взрыв с силой: " +
                    $"{_field[_playerPosition.XPos, _playerPosition.YPos]}\n");
                _field[_playerPosition.XPos, _playerPosition.YPos] = -1;
            }
            Notification?.Invoke($"Колл-во очков жизни: {_playerLifes}\n");

            for (int i=0;i<_fieldSize;i++)
            {
                for (int j = 0; j < _fieldSize; j++)
                {
                    if (i == _playerPosition.XPos && j == _playerPosition.YPos)
                    {
                        Notification?.Invoke("you ");
                        _field[i, j] = -1;
                    }
                    else
                    {
                        if (i == _finishPosition.XPos && j == _finishPosition.YPos)
                        {
                            Notification?.Invoke("fin ");
                        }
                        else
                        {
                            if (_field[i, j] == -1)
                            {
                                Notification?.Invoke("sfl ");
                            }
                            else
                            {
                                Notification?.Invoke("___ ");
                            }
                        }
                    }
                }
                Notification?.Invoke("\n");
            }
            Notification?.Invoke("\n");
        }

        private Direction GetDirection(ConsoleKey consoleKey)
        {
            Direction direction;
            if (consoleKey == ConsoleKey.UpArrow || consoleKey == ConsoleKey.W)
            {
                direction = Direction.Forward;
            }
            else
            {
                if (consoleKey == ConsoleKey.DownArrow || consoleKey == ConsoleKey.S)
                {
                    direction = Direction.Backward;
                }
                else
                {
                    if (consoleKey == ConsoleKey.LeftArrow || consoleKey == ConsoleKey.A)
                    {
                        direction = Direction.Left;
                    }
                    else
                    {
                        if (consoleKey == ConsoleKey.RightArrow || consoleKey == ConsoleKey.D)
                        {
                            direction = Direction.Right;
                        }
                        else
                        {
                            direction = Direction.None;
                        }
                    }
                }
            }
            return direction;
        }

        public bool Move(ConsoleKeyInfo consoleKeyInfo)
        {
            Direction direction=GetDirection(consoleKeyInfo.Key);
            if (direction==Direction.None)
            {
                return false;
            }
            
            switch (direction)
            {
                case Direction.Forward:
                    {
                        if(_playerPosition.XPos > 0)
                        {
                            _playerPosition.XPos -= 1;
                        }
                    }
                    break;
                case Direction.Backward:
                    {
                        if (_playerPosition.XPos < _fieldSize-1)
                        {
                            _playerPosition.XPos += 1;
                        }
                    }
                    break;
                case Direction.Left:
                    {
                        if (_playerPosition.YPos > 0)
                        {
                            _playerPosition.YPos -= 1;
                        }
                    }
                    break;
                case Direction.Right:
                    {
                        if (_playerPosition.YPos < _fieldSize-1)
                        {
                            _playerPosition.YPos += 1;
                        }
                    }
                    break;
            }
            if(_field[_playerPosition.XPos, _playerPosition.YPos] >0)
            {
                _playerLifes -= _field[_playerPosition.XPos, _playerPosition.YPos];
                _trapsNumber--;
            }
            return true;
        }

        public bool IsGameOver()
        {
            if (_playerLifes > 0 && _playerPosition != _finishPosition)
            {
                return false;
            }
            else
            {
                ConsolePrintField();
                if (_playerLifes <= 0)
                {
                    Notification?.Invoke("\nВы проиграли\n");
                }
                else
                {
                    Notification?.Invoke("\nВЫ ВЫИГРАЛИ!!!\n");                    
                }
                return true;
            }
        }
    }
}
