﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Module_5
{
    class PositionOnThePlane
    {
        private int X;
        private int Y;
        
        public int YPos
        {
            get
            {
                return Y;
            }
            set 
            {
                Y = value;
            } 
        }
        public int XPos
        {
            get
            {
                return X;
            }
            set
            {
                X = value;
            }
        }
        public PositionOnThePlane(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static bool operator ==(PositionOnThePlane position1, PositionOnThePlane position2)
        {
            return (position1.X == position2.X && position1.Y == position2.Y) ? true : false;
        }

        public static bool operator !=(PositionOnThePlane position1, PositionOnThePlane position2)
        {
            return (position1.X == position2.X && position1.Y == position2.Y) ? false : true;
        }
    }
}
