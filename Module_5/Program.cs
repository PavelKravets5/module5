﻿using System;

namespace Module_5
{
    class Program
    {
        static void Main(string[] args)
        {
            bool newGame = true;
            while(newGame)
            {
                Console.WriteLine("\nСтарт с верхнего левого угла поля " +
                    "(по умолчанию)? y/n/yes/no");
                string message = Console.ReadLine();
                int startPosNumber = 0;
                while (!Notification.CheckAgreementOrDisagreement(ref message))
                {
                    Console.WriteLine("\nНекорректно, еще раз");
                    message = Console.ReadLine();
                }
                if (message == "no" || message == "n")
                {
                    Console.WriteLine("Чтобы начать с верхнего левого угла поля, введите 0,\n" +
                        "Чтобы начать с верхнего правого угла поля, введите 1,\n" +
                        "Чтобы начать с нижнего левого угла поля, введите 2,\n" +
                        "Чтобы начать с нижнего правого угла поля, введите 3,\n");
                    while (!Notification.ReadWithCheck(Console.ReadLine(), Notification.LIMIT_0,
                        Notification.LIMIT_3, out startPosNumber))
                    {
                        Console.WriteLine("Некорректно, еще раз");
                    }
                }

                Console.WriteLine("\nРазмер поля = 10 (по умолчанию)? y/n/yes/no");
                message = Console.ReadLine();
                while (!Notification.CheckAgreementOrDisagreement(ref message))
                {
                    Console.WriteLine("\nНекорректно, еще раз");
                    message = Console.ReadLine();
                }

                int fieldSize = 10;
                if (message == "no" || message == "n")
                {
                    Console.WriteLine("Введите длинну строны игрового поля, оно квадратное " +
                        "(длинна стороны принадлежит интервалу [2,100]):");                
                    while (!Notification.ReadWithCheck(Console.ReadLine(), Notification.LIMIT_2,
                        Notification.LIMIT_100, out fieldSize))
                    {
                        Console.WriteLine("Некорректно, еще раз");
                    }
                }

                bool check=true;
                message = "no";
                if(fieldSize<4)
                {
                    Console.WriteLine("\nВы не можете выбрать число ловушек по " +
                        "умолчанию = 10, т.к. выбранный размер поля слишком мал.");
                    
                }
                else
                {
                    Console.WriteLine("\nКол-во ловушек = 10 (по умолчанию)? y/n/yes/no");
                    message = Console.ReadLine();
                    check = Notification.CheckAgreementOrDisagreement(ref message);
                }
                
                while (!check)
                {
                    Console.WriteLine("\nНекорректно, еще раз");
                    message = Console.ReadLine();
                    check = Notification.CheckAgreementOrDisagreement(ref message);
                }
                int trapsNumber = 10;
                if (message == "no" || message == "n")
                {
                    int maxTrapsNumber = (int)Math.Pow(fieldSize, 2) - 2;
                    Console.WriteLine($"Введите колличество ловушек, в пределах " +
                        $"от {Notification.LIMIT_0} до кол-во_клеток_поля - 2 = {maxTrapsNumber}:");
                    while (!Notification.ReadWithCheck(Console.ReadLine(), Notification.LIMIT_0,
                        maxTrapsNumber, out trapsNumber))
                    {
                        Console.WriteLine("Некорректно, еще раз");
                    }
                }

                Console.Clear();
                Game game = new Game(startPosNumber,fieldSize,trapsNumber);
                game.Notification += Notification.DisplayMessage;
                do
                {                    
                    game.ConsolePrintField();
                    Console.WriteLine("Ходите. Ожидание ввода клавиши WASD или стрелки:");
                    while (!game.Move(Console.ReadKey()))
                    {
                        Console.WriteLine("\nНекорректно, еще раз");
                    }
                    Console.Clear();
                } 
                while (!game.IsGameOver());

                Console.WriteLine("\nЕще партию? y/n/yes/no");
                message = Console.ReadLine();
                while (!Notification.CheckAgreementOrDisagreement(ref message))
                {
                    Console.WriteLine("\nНекорректно, еще раз");
                    message = Console.ReadLine();
                }
                newGame = message == "y"||message=="yes" ? true : false;
            }
        }
    }
}
